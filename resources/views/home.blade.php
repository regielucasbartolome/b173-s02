@extends('layouts.app_old')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Dashboard</h1></div>
                    <a href="/posts/create" class="btn btn-primary">Create Post</a>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h2>My Posts</h2>
                    @if(count($posts) > 0)
                        <table class="table table-striped">
                           <tr>
                               <th class="text-center">Title</th>
                               <th class="text-center">Ceated On</th>
                               <th class="text-center">Last Updated</th>
                               <th class="text-center" colspan="2">Action</th>
                           </tr>
                           @foreach($posts as $post)
                           <tr>
                               <td>{{ $post -> title }}</td>
                               <td>{{ $post -> created_at}}</td>
                               <td>{{ $post -> updated_at}}</td>
                               <td><a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit</a></td>
                               <td>
                                   <form method ="POST" action="/posts/{{$post->id}}">
                                    @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button class="btn btn-danger">DELETE</button>
                                   </form>
                               </td>
                           </tr>
                           @endforeach
                    @endif
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
